# Atividade Continua 4

Este repositório contém o código e os recursos relacionados à Atividade Continua 4.

## Autor
- Nome: Fábio Martins Vieira

## Descrição
A Atividade Continua 4 consiste em um projeto que envolve a criação de uma aplicação web Flask, a configuração de testes unitários e a implementação de pipelines de integração contínua (CI) com o GitLab. O projeto inclui as seguintes características:

- Uma aplicação web Flask simples que retorna uma mensagem de teste quando acessada.
- Testes unitários para verificar a funcionalidade da aplicação e a segurança do código.
- Uma pipeline de CI no GitLab que automatiza a construção da imagem Docker, a execução de testes e a implantação do contêiner.

## Conteúdo do Repositório
- `app.py`: O código-fonte da aplicação web Flask.
- `Dockerfile`: O arquivo Dockerfile para construir a imagem Docker da aplicação.
- `.gitlab-ci.yml`: O arquivo de configuração da pipeline de CI no GitLab.
- `tests/`: Um diretório que contém arquivos de teste unitário.
- `README.md`: Este arquivo de documentação.

## Executando a Aplicação Localmente
Você pode executar a aplicação localmente seguindo as instruções no arquivo `README.md` do repositório.

## Contribuições
Contribuições para este projeto são bem-vindas. Se você deseja contribuir, siga as diretrizes de contribuição do projeto.

## Licença
Este projeto está licenciado sob a Licença MIT - consulte o arquivo [LICENSE](LICENSE) para obter detalhes.

