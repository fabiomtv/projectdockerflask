import unittest
from app import app
from utils import calcular_algo
from utils import is_par

class TestApp(unittest.TestCase):
    def setUp(self):
        # Configurar uma instância de teste do aplicativo
        self.app = app.test_client()

    def test_rota_inicio(self):
        # Testar a rota principal
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_calculo_algo(self):
        # Testar a função calcular_algo
        resultado = calcular_algo(5, 10)
        self.assertEqual(resultado, 15)

    def test_is_par(self):
        # Testar a função is_par com um número par (deve retornar True)
        self.assertTrue(is_par(4))

        # Testar a função is_par com um número ímpar (deve retornar False)
        self.assertFalse(is_par(5))

    def test_rota_inexistente(self):
        # Testar uma rota inexistente
        response = self.app.get('/rota_inexistente')
        self.assertEqual(response.status_code, 404)

if __name__ == '__main__':
    unittest.main()
