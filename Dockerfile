# Use a imagem base Python 3.8
FROM python:3.8-slim

# Defina o diretório de trabalho no contêiner
WORKDIR /app

# Copie o arquivo app.py para o contêiner
COPY app.py /app/app.py

# Copie o diretório "tests" para o contêiner
COPY tests /app/tests

# Instale as dependências do projeto
RUN pip install Flask

# Exponha a porta em que a aplicação Flask estará ouvindo (por padrão, o Flask ouve na porta 5000)
EXPOSE 5000

# Defina a variável de ambiente FLASK_APP
ENV FLASK_APP=app.py

# Defina o host e a porta para a aplicação Flask e ative o ambiente virtual
CMD ["/bin/bash", "-c", "source venv/bin/activate && flask run --host=0.0.0.0 --port=5000"]
