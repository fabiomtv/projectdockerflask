# Função de cálculo simples
def calcular_algo(a, b):
    return a + b

# Função para verificar se um número é par
def is_par(numero):
    return numero % 2 == 0

# Classe de utilitário para manipular strings
class StringUtils:
    @staticmethod
    def inverter_string(texto):
        return texto[::-1]

# Outros utilitários, constantes, classes, etc.

is_par(4)