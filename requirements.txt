# Dependências gerais da aplicação
Flask==2.1.1
SQLAlchemy==1.4.22
requests==2.26.0
...

# Dependências para testes
pytest==6.2.5
coverage==6.2
...

# Dependências para segurança
semgrep==0.55.0
...
